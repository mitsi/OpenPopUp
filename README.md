# Open Pop UP
Display a pop up wherever you want !

## How does it work ?
A background service inflates a layout and displays it on the screen (and lockscreen).
The normal behavior is :
 - Display the layout
 - Disappear

But you can customize :
 - Add an Android notification
 - Make the layout draggable
 - Be always on top
 - Create your own behavior

To add a custom behavior you will need to bind the service and implement the before callback
## Example

```sh
public class MainActivity extends AppCompatActivity {

    private CustomNotificationService notificationService;
    private ServiceConnection nConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CustomNotificationService.CustomNotificationServiceBinder binder = (CustomNotificationService.CustomNotificationServiceBinder) service;
            notificationService = binder.getService();
            notificationService.notificationState = new NotificationState() {
                @Override
                public void before(View view) {
                    ((TextView)view.findViewById(R.id.tv_title)).setText("Hi :)");
                    ((TextView)view.findViewById(R.id.tv_content)).setText("How you going ?");
                }
            };
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            notificationService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(MainActivity.this, CustomNotificationService.class));
        Button b = (Button) findViewById(R.id.button);

        final PopUp.PopUpBuilder popUpBuilder = new PopUp.PopUpBuilder();
        popUpBuilder.contentTitle("Hi :)")
                .contentText("How you going ?")
                .countDown(1000)
                .withAndroidNotification(true)
                .popupLayout(R.layout.popup_layout);

        // If need to interact with the custom layout:
        Intent bindingIntent = new Intent(this, CustomNotificationService.class);
        bindService(bindingIntent, nConnection, Context.BIND_AUTO_CREATE);

        // UI
        final Intent intent = new Intent(MainActivity.this, CustomNotificationService.class);
        intent.putExtra(CustomNotificationService.FLAG_DISPLAY, popUpBuilder.build());
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(intent);
            }
        });
    }
}
```