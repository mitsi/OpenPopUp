package fr.simple.mitsichury.openpopup;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import java.util.Date;

public class CustomNotificationService extends Service {

    public static final String FLAG_DISPLAY = "DISPLAY";

    private static final String TAG = "PopUpService";

    private WindowManager windowManager;
    private LayoutInflater inflater;
    private View view;
    private LayoutParams params;
    public NotificationState notificationState = null;
    private PopUp popUp;

    //Handle gesture
    private int _xDelta;
    private int _yDelta;
    private long tmpDate = new Date().getTime();

    @Override
    public IBinder onBind(Intent intent) {
        return new CustomNotificationServiceBinder();
    }

    public class CustomNotificationServiceBinder extends Binder {
        public CustomNotificationService getService() {
            return CustomNotificationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        inflater = (LayoutInflater) getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            params = new LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.TYPE_APPLICATION_OVERLAY,
                    // Keeps the button presses from going to the background window
                    LayoutParams.FLAG_NOT_FOCUSABLE |
                            // Enables the notification to recieve touch events
                            LayoutParams.FLAG_NOT_TOUCH_MODAL |
                            LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            LayoutParams.FLAG_DISMISS_KEYGUARD |
                            // Draws over status bar
                            LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new LayoutParams(
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT,
                    LayoutParams.TYPE_SYSTEM_ERROR,
                    // Keeps the button presses from going to the background window
                    LayoutParams.FLAG_NOT_FOCUSABLE |
                            // Enables the notification to recieve touch events
                            LayoutParams.FLAG_NOT_TOUCH_MODAL |
                            // Draws over status bar
                            LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                    PixelFormat.TRANSLUCENT);
        }

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.windowAnimations = android.R.style.Animation_Translucent;
        params.x = 0;
        params.y = 0;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getExtras() != null) {
            this.popUp = (PopUp) intent.getSerializableExtra(FLAG_DISPLAY);
            display();
        }
        return START_NOT_STICKY;
    }

    /**
     * Display the PopUp
     * If a PopUp already exists, the new replace the old one
     */
    public void display() {
        if (!checkIfCanDrawOverlays()) {
            Toast.makeText(getApplicationContext(), "Permission denied...", Toast.LENGTH_SHORT).show();
            return;
        }
        dismiss();
        view = inflater.inflate(popUp.popupLayout, null);

        if (popUp.isDraggable) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int X = (int) event.getRawX();
                    final int Y = (int) event.getRawY();
                    boolean isConsumed = false;
                    LayoutParams layoutParams = (LayoutParams) v.getLayoutParams();
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                            _xDelta = X - layoutParams.x;
                            _yDelta = Y - layoutParams.y;
                            break;
                        case MotionEvent.ACTION_UP:
                            if (new Date().getTime() - tmpDate < 200) {
                                forceDismiss();
                                isConsumed = true;
                            }
                            tmpDate = new Date().getTime();
                        case MotionEvent.ACTION_POINTER_DOWN:
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            layoutParams.x = X - _xDelta;
                            layoutParams.y = Y - _yDelta;
                            windowManager.updateViewLayout(view, layoutParams);
                            isConsumed = !(new Date().getTime() - tmpDate < 50);
                            break;
                    }
                    Log.i("ISCONSUMED", isConsumed + "");
                    return isConsumed;
                }
            });
        }

        windowManager.addView(view, params);
        if (notificationState != null) {
            notificationState.before(view);
        }
        if (popUp.withAndroidNotification) {
            createNotification(popUp);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, popUp.countDown);
    }

    private boolean checkIfCanDrawOverlays() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(CustomNotificationService.this);
    }

    private void dismiss() {
        if (view != null && !popUp.alwaysDisplayed) {
            windowManager.removeView(view);
            view = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        forceDismiss();
    }

    private void createNotification(PopUp popUp) {
        Notification.Builder notificationBuilder = new Notification.Builder(getApplicationContext())
                .setContentTitle(popUp.contentTitle)
                .setContentText(popUp.contentText)
                .setSmallIcon((popUp.icon != -1) ? popUp.icon : R.mipmap.ic_launcher)
                .setPriority(popUp.priority)
                .setVibrate(popUp.vibrate)
                .setSound(popUp.sound);

        if (popUp.progress != -1 && popUp.progressMax != -1) {
            notificationBuilder.setProgress(popUp.progress, popUp.progressMax, popUp.isInfinite);
        }


        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(popUp.priority, notificationBuilder.build());
    }

    /**
     * Return the view that was inflated
     */
    public View getInflatedLayoutView() {
        return view;
    }

    /**
     * Return the window manager of the service
     * Can be useful if you want to change the position for example
     */
    public WindowManager getWindowManager() {
        return windowManager;
    }

    /**
     * Een if f the flag alwaysDisplayed is set to true, it hides the PopUp
     */
    public void forceDismiss() {
        if (popUp == null){
            return;
        }
        popUp.alwaysDisplayed = false;
        dismiss();
    }

}