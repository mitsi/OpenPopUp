package fr.simple.mitsichury.openpopup;

import android.view.View;

import java.io.Serializable;

/**
 * Created by MITSICHURY on 11/12/2016.
 */
public interface INotificationState{
    /**
     * Customize the PopUp behavior
     * @param view
     *      The view that was inflated by the layout you chose
     */
    void before(View view);
}
