package fr.simple.mitsichury.openpopup;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private CustomNotificationService notificationService;
    private ServiceConnection nConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CustomNotificationService.CustomNotificationServiceBinder binder = (CustomNotificationService.CustomNotificationServiceBinder) service;
            notificationService = binder.getService();
            notificationService.notificationState = new NotificationState() {
                @Override
                public void before(View view) {
                    ((TextView) view.findViewById(R.id.tv_title)).setText("Hi :)");
                    ((TextView) view.findViewById(R.id.tv_content)).setText("How you going ?");
                }
            };
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            notificationService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(MainActivity.this, CustomNotificationService.class));
        Button b = findViewById(R.id.button);

        final PopUp.PopUpBuilder popUpBuilder = new PopUp.PopUpBuilder();
        popUpBuilder.contentTitle("Hi :)")
                .contentText("How you going ?")
                .alwaysDisplayed(true)
                .withAndroidNotification(true)
                .popupLayout(R.layout.popup_layout);

        // If need to interact with the custom layout:
        Intent bindingIntent = new Intent(this, CustomNotificationService.class);
        bindService(bindingIntent, nConnection, Context.BIND_AUTO_CREATE);

        // UI
        final Intent intent = new Intent(MainActivity.this, CustomNotificationService.class);
        intent.putExtra(CustomNotificationService.FLAG_DISPLAY, popUpBuilder.build());
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.canDrawOverlays(MainActivity.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 1);
                    } else {
                        startService(intent);
                    }
                } else {
                    startService(intent);
                }
            }
        });
    }
}
