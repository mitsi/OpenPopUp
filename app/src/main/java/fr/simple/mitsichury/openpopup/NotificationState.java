package fr.simple.mitsichury.openpopup;

import android.view.View;

import java.io.Serializable;

/**
 * Created by MITSICHURY on 11/12/2016.
 */
public abstract class NotificationState implements INotificationState{
    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void before(View view);
}
