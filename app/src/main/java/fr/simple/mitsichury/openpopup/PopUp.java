package fr.simple.mitsichury.openpopup;

import android.app.Notification;
import android.net.Uri;
import android.text.Layout;
import android.view.View;

import java.io.Serializable;

/**
 * Created by MITSICHURY on 11/12/2016.
 */
public class PopUp implements Serializable {
    protected int popupLayout;
    protected String contentTitle;
    protected String contentText;
    protected int priority;
    protected int progress;
    protected int progressMax;
    protected boolean isInfinite;
    protected long[] vibrate;
    protected Uri sound;
    protected boolean withAndroidNotification;
    protected int icon;
    protected int countDown;
    protected boolean alwaysDisplayed;
    protected boolean isDraggable;


    private PopUp(){
    }

    public static final class PopUpBuilder implements Serializable {
        private int icon = -1;
        private Uri sound = null;
        private boolean withAndroidNotification = false;
        private long[] vibrate = null;
        private boolean isInfinite = false;
        private int progressMax = -1;
        private int progress = -1;
        private int priority = Notification.PRIORITY_DEFAULT;
        private String contentText = "";
        private String contentTitle = "";
        private int popupLayout = R.layout.popup_layout;
        private int countDown = 1000;
        private boolean alwaysDisplayed = false;
        private boolean isDraggable = false;

        public PopUpBuilder() {
        }

        /**
         * Hides the PopUp after the delay
         * @param milliseconds
         *      The delay milliseconds
         */
        public PopUpBuilder countDown(int milliseconds) {
            this.countDown = milliseconds;
            return this;
        }

        /**
         * Sets whether the popup is always displayed or not
         * @param alwaysDisplayed
         */
        public PopUpBuilder alwaysDisplayed(boolean alwaysDisplayed) {
            this.alwaysDisplayed = alwaysDisplayed;
            return this;
        }

        /**
         * The popUp become draggable
         * CAUTION : you can't add all event listeners in child view
         * Bind the service and make your own implementation for custom behavior
         * @param isDraggable
         */
        public PopUpBuilder isDraggable(boolean isDraggable) {
            this.isDraggable = isDraggable;
            return this;
        }

        /**
         * The layout to inflate
         * @param popupLayout
         *      The layout resource id
         */
        public PopUpBuilder popupLayout(int popupLayout) {
            this.popupLayout = popupLayout;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the title of the Android's notification
         * @param contentTitle
         *      The title
         */
        public PopUpBuilder contentTitle(String contentTitle) {
            this.contentTitle = contentTitle;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the content of the Android's notification
         * @param contentText
         *      The content
         */
        public PopUpBuilder contentText(String contentText) {
            this.contentText = contentText;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the priority of the Android's notification
         * @param priority
         *      The notification's priority
         *      Default is Notification.PRIORITY_DEFAULT (0)
         */
        public PopUpBuilder priority(int priority) {
            this.priority = priority;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the progress of the Android's notification
         * @param progress
         *      Actual progress
         * @param progressMax
         *      The maximum
         * @param isInfinite
         *      If the progress animation is infinite
         */
        public PopUpBuilder progress(int progress, int progressMax, boolean isInfinite) {
            this.progress = progress;
            this.progressMax = progressMax;
            this.isInfinite = isInfinite;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the vibrate pattern of the Android's notification
         * @param vibrate
         *      The pattern
         */
        public PopUpBuilder vibrate(long[] vibrate) {
            this.vibrate = vibrate;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the sound to play of the Android's notification
         * @param sound
         *      The Uri to the soound file
         */
        public PopUpBuilder sound(Uri sound) {
            this.sound = sound;
            return this;
        }

        /**
         * If want to use the android notification with the popup
         * @param withAndroidNotification
         *      If you want to display the normal Android's notification
         */
        public PopUpBuilder withAndroidNotification(boolean withAndroidNotification) {
            this.withAndroidNotification = withAndroidNotification;
            return this;
        }

        /**
         * If you want to use the Android's notification
         * It sets the icon of the Android's notification
         * @param icon
         *      The icon resource id
         * @return
         */
        public PopUpBuilder icon(int icon) {
            this.icon = icon;
            return this;
        }

        /**
         * Build the PopUp object
         * @return
         *      The PopUp Object to pass to the service
         */
        public PopUp build() {
            PopUp popUp = new PopUp();
            popUp.contentTitle = this.contentTitle;
            popUp.priority = this.priority;
            popUp.sound = this.sound;
            popUp.isInfinite = this.isInfinite;
            popUp.popupLayout = this.popupLayout;
            popUp.progressMax = this.progressMax;
            popUp.progress = this.progress;
            popUp.withAndroidNotification = this.withAndroidNotification;
            popUp.contentText = this.contentText;
            popUp.icon = this.icon;
            popUp.vibrate = this.vibrate;
            popUp.countDown = this.countDown;
            popUp.alwaysDisplayed = this.alwaysDisplayed;
            popUp.isDraggable = this.isDraggable;
            return popUp;
        }
    }
}
